Rails.application.routes.draw do
  devise_for :users
  resources :forum_threads do
    resources :posts
  end

  get 'tdestroy_all' => 'forum_threads#destroy_all'

  root 'forum_threads#index'
end
