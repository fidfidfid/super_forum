json.extract! post, :id, :content, :nick_name, :email, :forum_thread_id, :created_at, :updated_at
json.url post_url(post, format: :json)
