# == Schema Information
#
# Table name: tenants
#
#  id         :integer          not null, primary key
#  host_name  :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Tenant < ApplicationRecord
end
