# == Schema Information
#
# Table name: posts
#
#  id              :integer          not null, primary key
#  content         :text
#  forum_thread_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer
#
# Indexes
#
#  index_posts_on_forum_thread_id  (forum_thread_id)
#  index_posts_on_user_id          (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (forum_thread_id => forum_threads.id)
#

class Post < ApplicationRecord
  belongs_to :forum_thread
  belongs_to :user

  validates :content, presence: true
end
