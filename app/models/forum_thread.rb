# == Schema Information
#
# Table name: forum_threads
#
#  id         :integer          not null, primary key
#  title      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#  tenant_id  :integer
#
# Indexes
#
#  index_forum_threads_on_tenant_id  (tenant_id)
#  index_forum_threads_on_user_id    (user_id)
#

class ForumThread < ApplicationRecord
  has_many :posts, dependent: :destroy
  belongs_to :user

  accepts_nested_attributes_for :posts

  default_scope { where(tenant_id: Thread.current[:tenant_id]) }

  validates :title, presence: true
end
