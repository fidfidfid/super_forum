class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_lang
  before_action :set_tz
  before_action :set_tenant

  def set_lang
    I18n.locale = 'id'
  end

  def set_tz
    Time.zone = 'Asia/Jakarta'
  end

  def set_tenant
    tenant = Tenant.find_or_create_by(host_name: request.host)
    Thread.current[:tenant_id] = tenant.id
  end

end
