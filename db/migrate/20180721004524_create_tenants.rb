class CreateTenants < ActiveRecord::Migration[5.1]
  def change
    create_table :tenants do |t|
      t.string :host_name

      t.timestamps
    end

    add_reference :forum_threads, :tenant, index: true
    add_reference :users, :tenant, index: true
  end
end
