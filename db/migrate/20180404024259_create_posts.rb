class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.text :content
      t.string :nick_name
      t.string :email
      t.belongs_to :forum_thread, foreign_key: true

      t.timestamps
    end
  end
end
