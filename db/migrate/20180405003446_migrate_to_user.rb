class MigrateToUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :posts, :user, index: true
    add_reference :forum_threads, :user, index: true
    remove_column :posts, :nick_name, :string
    remove_column :posts, :email, :string
  end
end
